<?php if ($page['preface_first'] || $page['preface_middle'] || $page['preface_last']): ?>
  <div id="preface-block-wrap" class="clr">
    <?php if($page['preface_first']): ?><div class="span_1_of_3 col col-1 preface-block ">
      <?php print render ($page['preface_first']); ?>
    </div><?php endif; ?>
    <?php if($page['preface_middle']): ?><div class="span_1_of_3 col col-2 preface-block ">
      <?php print render ($page['preface_middle']); ?>
    </div><?php endif; ?>
    <?php if($page['preface_last']): ?><div class="span_1_of_3 col col-3 preface-block ">
      <?php print render ($page['preface_last']); ?>
    </div><?php endif; ?>
  </div>
<?php endif; ?>