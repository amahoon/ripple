<?php if ($page['content_suffix']): ?>
  <div id="wrap" class="clr container">
    <div id="content-suffix">
      <?php print render($page['content_suffix']); ?>
    </div>
	</div>
<?php endif; ?>