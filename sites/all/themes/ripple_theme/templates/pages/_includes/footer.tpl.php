<!-- <div id="wrap" class="clr container"> -->
<footer>
  <div id="footer-container">
    <div id="wrap" class="clr container">
      <?php if ($page['footer_first'] || $page['footer_second'] || $page['footer_third']  || $page['footer']): ?>
        <div id="footer-wrap" class="site-footer clr container">
          <div id="footer" class="clr">
            <?php if ($page['footer_first'] || $page['footer_second'] || $page['footer_third']): ?>
              <div id="footer-block-wrap" class="clr">
                <?php if($page['footer_first']): ?><div class="span_1_of_3 col col-1 footer-block ">
                  <?php print render ($page['footer_first']); ?>
                </div><?php endif; ?>
                <?php if($page['footer_second']): ?><div class="span_1_of_3 col col-2 footer-block ">
                  <?php print render ($page['footer_second']); ?>
                </div><?php endif; ?>
                <?php if($page['footer_third']): ?><div class="span_1_of_3 col col-3 footer-block ">
                  <?php print render ($page['footer_third']); ?>
                </div><?php endif; ?>
              </div>
            <?php endif; ?>
            
            <?php if ($page['footer']): ?>
              <div class="span_1_of_1 col col-1">
                <?php print render($page['footer']); ?>
              </div>
            <?php endif; ?>
          </div>
        </div>
      <?php endif; ?>

      <?php require_once 'featured-artist.tpl.php'; ?>

      <!-- <footer id="copyright-wrap">
        <div id="copyright"><?php // print t('Copyright'); ?> &copy; 2015 - <?php // echo date("Y"); ?> <a href="<?php // print $front_page; ?>"><?php // print $site_name; ?></a>. <?php // print t('Designed and developed by'); ?>  <a href="http://alyssa-mahon.ca/" title="Alyssa Mahon" target="_blank">Alyssa Mahon</a>.</div>
      </footer> -->
    </div>
  </div>

  <div id="copyright"><?php print t('Copyright'); ?> &copy; <?php echo date("Y"); ?> <a href="<?php print $front_page; ?>"><?php print $site_name; ?></a></div>
</div>