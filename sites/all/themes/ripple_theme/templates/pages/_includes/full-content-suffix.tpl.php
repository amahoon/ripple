<?php if ($page['full_content_suffix']): ?>
  <div id="full-content-suffix" style="background: url(<?php print $featured_image_path; ?>) center/100% no-repeat fixed;">
  	<div id="wrap" class="clr container">
  	 <?php print render($page['full_content_suffix']); ?>
    </div>
    <div class="desaturate"></div>
  </div>
<?php endif; ?>