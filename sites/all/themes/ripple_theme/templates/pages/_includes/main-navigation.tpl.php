<div id="sidr-close"><a href="#sidr-close" class="toggle-sidr-close"></a></div>
<div id="site-navigation-wrap">
  <a href="#sidr-main" id="navigation-toggle"><span class="fa fa-bars"></span>Menu</a>
  <nav id="site-navigation" class="navigation main-navigation clr" role="navigation">
    <div class="green-holder"></div>
      <div id="main-menu" class="menu-main-container container">
        <div id="site-logo">
          <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>">
            <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
          </a>
        </div>
        <form id="search" name="search" action="index.php" method="get"><input type="text" name="livesearch" onkeyup="showResults(this.value)" id="livesearch" class="livesearch" placeholder="search"></form>
        <?php 
          $main_menu_tree = menu_tree(variable_get('menu_main_links_source', 'main-menu'));
          print drupal_render($main_menu_tree);
        ?>
      </div>
  </nav>
</div>