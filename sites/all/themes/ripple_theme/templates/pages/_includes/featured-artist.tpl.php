<?php if (isset($featured_artist)): ?>
  <div class="featured-DA-artist">
    <a class="featured-artist-picture" href="<?php print render($featured_artist->profile_url); ?>" target="_blank">
      <img id="DA-username--<?php print $featured_artist->user->username; ?>" class="DA-artist" src="<?php print $featured_artist->user->usericon; ?>" alt="<?php (isset($featured_artist->real_name) && !empty($featured_artist->real_name)) ? print $featured_artist->real_name . ' (' . $featured_artist->user->username . ')' : print $featured_artist->user->username; ?>'s Profile Picture">
    </a>
    <p>Featured Artwork by <a href="<?php print render($featured_artist->profile_url); ?>" target="_blank"><?php print render($featured_artist->user->username); ?></a> on <a href="http://www.deviantart.com" target="_blank">DeviantArt</a></p>
  </div>
<?php endif; ?>  