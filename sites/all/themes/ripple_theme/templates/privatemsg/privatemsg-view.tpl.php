<?php
// Each file loads it's own styles because we cant predict which file will be
// loaded.
drupal_add_css(drupal_get_path('module', 'privatemsg') . '/styles/privatemsg-view.base.css');
drupal_add_css(drupal_get_path('module', 'privatemsg') . '/styles/privatemsg-view.theme.css');
?>
<?php if ($first_message || !$same) : ?>
  <?php if (!$same && !$first_message) : ?>
      </div>
    </div>
  </div>
  <?php endif; ?>
  <?php print $anchors; ?>
  <div <?php if ( !empty($message_classes)) { ?>class="<?php echo implode(' ', $message_classes); ?>" <?php } ?> id="privatemsg-mid-<?php print $mid; ?>">
    <div class="privatemsg-author-avatar">
      <?php print $author_picture; ?>
    </div>
    <div class="privatemsg-message-column">
      <?php if (isset($new)): ?>
        <!-- <span class="new privatemsg-message-new"><?php print $new ?></span> -->
      <?php endif ?>
      <div class="privatemsg-message-information">
        <span class="privatemsg-author-name"><?php print $author_name_link; ?></span> <span class="privatemsg-message-date"><?php print $message_timestamp; ?></span>
      </div>
      <div class="privatemsg-message-body">
<?php endif; ?>

        <?php print $anchors; ?>
        <div class="message-cont<?php if (isset($new)): ?> new<?php endif; ?>">
          <div class="position-datetime">
            <div class="message-body">
              <?php print $message_body; ?>
            </div>
            <div class="privatemsg-message-datetime-holder">
              <div class="center-datetime">
                <span class="privatemsg-message-datetime"><?php print $message_timestamp; ?></span>
              </div>
            </div>
          </div>
        </div>

<?php if ($last_message) : ?>
      </div>
    </div>
  </div>
<?php endif; ?>





