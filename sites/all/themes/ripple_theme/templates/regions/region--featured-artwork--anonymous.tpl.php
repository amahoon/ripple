<?php if ($is_front) : ?>
<div class="<?php print $classes; ?>" style="background: url(<?php print $featured_image_path; ?>) center/100% no-repeat fixed;">
	<div id="anonymous-user-menu" class="container">
		<ul class="menu">
			<li><a id="sign-in">sign in</a></li>
			<li><a id="sign-up">sign up</a></li>
		</ul>
		<?php print render($user_login_block); ?>
	</div>
	<div class="featured-artwork container">
	  	<?php if (!empty($content)) : ?>
			<div class="featuredart-title">
				<img id="site-logo" src="<?php print render($logo); ?>" alt="Ripple logo">
				<h1><?php print t('Welcome to '); ?><?php print $name; ?></h1>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at interdum nunc. Maecenas id neque sed orci pharetra euismod 
					ut et purus. Etiam tincidunt eros vel leo blandit, tristique pellentesque velit rhoncus. Sed facilisis non est vel finibus. 
					Proin convallis porta luctus.</p>
				<div class="main-arrow"></div>
			</div>
		<?php endif; ?>
	    <div class="desaturate"></div>
	</div>
</div>
<?php endif; ?>
<!-- /.region -->
