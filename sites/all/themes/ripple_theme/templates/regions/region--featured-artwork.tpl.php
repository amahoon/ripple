<div class="<?php print $classes; ?>" style="background: url(<?php print $featured_image_path; ?>) center/100% no-repeat fixed;">
	<div class="featured-artwork">
	  	<?php if (!empty($content)) : ?>
			<div class="featuredart-title">
				<h1><?php print t('Welcome to '); ?><?php print $name; ?></h1>
			</div>
		<?php endif; ?>
	    <div class="desaturate"></div>
	</div>
</div>
<!-- /.region -->
