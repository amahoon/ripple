<div class="<?php print $classes; ?>" style="background: url(<?php if (is_null($user_info['user_banner_path'])) { print $featured_image_path; } else { print $user_info['user_banner_path']; } ?>) center/100% no-repeat fixed;">
	<div class="featured-artwork">
		<?php if (!empty($content)) : ?>
			<div class="user-info <?php print $user_info['info_class']; ?>">
				<div class="profile-picture">
					<span class="holder">
						<?php print $user_info['user_picture']; ?>
						<?php if ($user_info['privatemsg'] && $user_info['privatemsg']['#options']['query']['destination'] != 'user/'.$user_info['logged_in_user']->uid) : ?>
							<a href="<?php print $user_info['privatemsg']['link']; ?>" class="<?php print $user_info['privatemsg']['#options']['attributes']['class']; ?>"></a>
						<?php endif; ?>
					</span>
				</div>
				<h1 class="page-title given-name"><?php if (!empty($user_info['user']->field_first_name)) { print $user_info['user']->field_first_name[LANGUAGE_NONE][0]['safe_value']; } ?> <?php if (!empty($user_info['user']->field_last_name)) { print $user_info['user']->field_last_name[LANGUAGE_NONE][0]['safe_value']; } ?></h1>
				<p class="username">@<?php print $user_info['user']->name; ?></p>
				<?php print flag_create_link('friend', $user_info['user']->uid); ?>
			</div>
		<?php endif; ?>
		<div class="desaturate"></div>
	</div>
</div>
<!-- /.region -->