<?php

/*
 * Helper function to check if episodes exist for the current node
 */
function _check_anime_has_episodes($id) {
	// $query = 'SELECT * FROM field_data_field_anime WHERE field_anime_target_id = 1 AND bundle = "episode"';
	$query = db_select('field_data_field_anime', 'anime');
	$query->condition('anime.field_anime_target_id',$id)
		->condition('anime.bundle', 'episode')
		->fields('anime');
	$result = $query->execute();
	return $result;
}

/**
 * Implements hook_preprocess_page().
 */
function ripple_theme_preprocess_page(&$variables) {
	GLOBAL $user;
	GLOBAL $base_url;

  $featured_artist = variable_get('featured_artist');
  
  if (isset($featured_artist->user)) {
    $variables['featured_artist'] = $featured_artist;
  }

	$variables['featured_piece'] = '';
	$variables['featured_image_path'] = '';
	if(!empty(theme_get_setting('featured_artwork'))) {
		$id = theme_get_setting('featured_artwork');
		$variables['featured_piece'] = theme('image', array(
			'path'  => file_create_url(file_load($id)->uri),
			'alt'   => t('Featured Artwork'),
			'title' => t('Featured Artwork'),
			'type'
		));
		$variables['featured_image_path'] = file_create_url(file_load($id)->uri);
	}

	$_SESSION['pm_mid'] = NULL;
	$user_login = block_load('user', 'login');
	// set up the user based on profile
	if (menu_get_object('user',1,menu_get_item()['tab_root_href'])) {
		$account = menu_get_object('user',1,menu_get_item()['tab_root_href']);
	} else {
		// if user is not available in the path, used the logged in user
		$account = $user;
	}

	$user_profile_path = 'user/' . $account->uid;
	if (current_path() === $user_profile_path) {
		$user_load = user_load($account->uid);
		$name = array();
		if ($user_load->field_first_name[LANGUAGE_NONE][0]['value'] || $user_load->field_last_name[LANGUAGE_NONE][0]['value']) {
			if ($user_load->field_first_name[LANGUAGE_NONE][0]['value']) {
				$name['fname'] = $user_load->field_first_name[LANGUAGE_NONE][0]['value'];
			}
			if ($user_load->field_last_name[LANGUAGE_NONE][0]['value']) {
				$name['lname'] = $user_load->field_last_name[LANGUAGE_NONE][0]['value'];
			}
		} else {
			$name['default'] = $user_load->name;
		}

		$variables['title'] = t("@user's Profile", array('@user' => implode(' ',$name)));
	} else {
		$variables['title'] = drupal_get_title();
	}

	if ($user->uid == 0) {
		$pages = array('frontpage','user','user/login','user/password','user/register','search/node/'.arg(2));
		if (!in_array(current_path(), $pages) && strpos(current_path(),'sites/default/files/') === FALSE) {
			$path = current_path();
			$title = drupal_get_title();
			drupal_set_message(t('You are not permitted to view the <i>@title</i> page. <a href="' . $base_url . '/user/login">Login</a> or <a href="' . $base_url . '/user/register">Sign Up</a> if you wish to proceed', array('@title' => $title)), 'error', FALSE);
			drupal_goto('<front>');
		}
		if (current_path() == 'frontpage') {
			$variables['theme_hook_suggestions'][] = 'page__front__anonymous';
		}
	}

	// if user can send private messages
	if (isset($variables['page']['content']['system_main']['privatemsg_send_new_message'])) {
		// add private message link to preprocess_region array
		$variables['user']->privatemsg = $variables['page']['content']['system_main']['privatemsg_send_new_message'];
	}
	$path = current_path();
	// hide the title from certain content types
	$types = array('anime','manga');
	if (isset($variables['node'])) {
		if (in_array($variables['node']->type, $types)) {
			$variables['hide_title'] = TRUE;
		} else {
			$variables['hide_title'] = FALSE;
		}
	} else {
		$variables['hide_title'] = FALSE;
	}
	// check to see if user is logged in
	if ($path === 'user' && $variables['user']->uid === 0) {
		$variables['logged_in'] = FALSE;
	} else {
		$variables['logged_in'] = TRUE;
	}
}

/**
 * Implements hook_preprocess_region()
 */
function ripple_theme_preprocess_region(&$variables) {
	GLOBAL $base_url;
	GLOBAL $user;

	if ($variables['region'] == 'featured_artwork') {
		// get featured artwork from theme setting
		$variables['name'] = variable_get('site_name');
		$variables['featured_piece'] = '';
		$variables['featured_image_path'] = '';
		if(!empty(theme_get_setting('featured_artwork'))) {
			$id = theme_get_setting('featured_artwork');
			$variables['featured_piece'] = theme('image', array(
				'path'  => file_create_url(file_load($id)->uri),
				'alt'   => t('Featured Artwork'),
				'title' => t('Featured Artwork'),
				'type'
			));
			$variables['featured_image_path'] = file_create_url(file_load($id)->uri);
		}
		if ($user->uid == 0) {
			$variables['theme_hook_suggestions'][] = 'region__featured_artwork__anonymous';
			$variables['logo'] = theme_get_setting('logo');
      $variables['user_login_block'] = _block_get_renderable_array(_block_render_blocks(array(block_load('user', 'login'))));
		}

		$path = current_path();
		$path_pieces = explode('/', $path);
		foreach ($path_pieces as $piece) {
			if (!is_numeric($piece)) {
				// add theme suggestion to the featured artwork region based on the path as long as it's not a number
				$variables['theme_hook_suggestions'][] = 'region__' . $variables['region'] . '__' . $piece;
			}
		}

		// set up the user based on profile
		if (menu_get_object('user',1,menu_get_item()['tab_root_href'])) {
			$account = menu_get_object('user',1,menu_get_item()['tab_root_href']);
		} else {
			// if user is not available in the path, used the logged in user
			$account = $user;
		}

		// variables for the featured artwork region for user oriented pages
		if (strpos($path, 'user/') !== false 
			|| strpos($path, 'messages') !== false
			|| strpos($path, 'messages/') !== false) {
			$this_user = user_load($account->uid);
			if ($this_user->picture) {
				$profile_picture = '<img typeof="foaf:Image" src="'.image_style_url('profile_picture', $this_user->picture->uri).'" alt="'.$this_user->name.'\'s profile picture">';
			} else {
				$profile_picture = '<img src="'.$base_url.'/sites/default/files/user/default-user.png" alt="'.$this_user->name.'\'s profile picture">';
			}

			if (!empty($this_user->field_user_banner)) {
				$banner_alt = $this_user->name . '\'s banner';
				if (!empty($this_user->field_user_banner[LANGUAGE_NONE][0]['alt'])) {
					$banner_alt = $this_user->field_user_banner[LANGUAGE_NONE][0]['alt'];
				}
				$banner = '<img typeof="foaf:Image" src="'.image_style_url('user_banner', $this_user->field_user_banner[LANGUAGE_NONE][0]['uri']).'" alt="'.$banner_alt.'">';
				$banner_path = file_create_url($this_user->field_user_banner[LANGUAGE_NONE][0]['uri']);
			} else {
				$banner = NULL;
				$banner_path = NULL;
			}

			if (isset($variables['user']->privatemsg)) {
				$privatemsg = $variables['user']->privatemsg;
				$privatemsg['link'] = $base_url . '/' . $privatemsg['#href'] . '?destination=' . $privatemsg['#options']['query']['destination'];
			} elseif ($account->uid != $user->uid) {
				$privatemsg['#title'] = t('Send this user a private message');
				$privatemsg['#href'] = 'messages/new/' . $account->uid;
				$privatemsg['#options'] = array(
					'query' => array(
						'destination' => 'user/' . $account->uid,
					),
					'title' => $privatemsg['#title'],
					'attributes' => array(
						'class' => 'privatemsg-send-link privatemsg-send-link-profile',
					),
				);
				$privatemsg['link'] = $base_url . '/' . $privatemsg['#href'] . '?destination=' . $privatemsg['#options']['query']['destination'];
			} else {
				$privatemsg = NULL;
			}
			if ($account->uid == $user->uid) {
				$info_class = 'logged_in_user';
			} else {
				$info_class = '';
			}
			$variables['user_info'] = array(
				'info_class' => $info_class,
				'base_url' => $base_url,
				'logged_in_user' => $user,
				'user' => $this_user,
				'user_picture' => $profile_picture,
				'user_banner' => $banner,
				'user_banner_path' => $banner_path,
				'privatemsg' => $privatemsg,
			);
		}
	}
}

/**
 * Implements hook_preprocess_node()
 */
function ripple_theme_preprocess_node(&$variables) {
	// hide read count from nodes (used for sitesuper who can't disable the permission)
	unset($variables['content']['links']['statistics']['#links']['statistics_counter']['title']);

	// variables for node--anime.tpl.php
	$variables['episodes'] = '';
	$variables['link_to_manga'] = 'false';
	$variables['show_promo'] = FALSE;

	/*
	 * Seasons and Episodes display setup
	 */
	if (!empty($variables['field_number_of_seasons'])) {
		if ($variables['field_number_of_seasons'][0]['value'] > 1) {
			// only show season if it's greater than one
			$variables['seasons'] = '<div class="field-label">Season(s): </div><div class="field-items">' . $variables['field_number_of_seasons'][0]['value'] . '</div>';
		}
	}
	if (!empty($variables['field_number_of_episodes'])) {
		if ($variables['field_number_of_episodes'][0]['value'] == 0) {
			// if episode number is set to 0, the display is unknown
			$variables['episodes'] = '<div class="field-label">Episode(s): </div><div class="field-items">unknown</div>';
		} else {
			$variables['episodes'] = '<div class="field-label">Episode(s): </div><div class="field-items">' . $variables['field_number_of_episodes'][0]['value'] . '</div>';
		}
	}

	/*
	 * Create link to related manga if it exists
	 */
	if (!empty($variables['field_related_manga'])) {
		$nid = $variables['field_related_manga'][0]['entity']->nid;
		$path = drupal_get_path_alias('node/' . $nid);
		$variables['link_to_manga'] = base_path() . $path;
	}

	/*
	 * Show or hide promotional videos
	 */
	$results = _check_anime_has_episodes($variables['nid']);
	$count = 0;
	foreach ($results as $result) {
		$count++;
	}
	if (!empty($variables['field_status']) && $variables['field_status'][0]['value'] == 'upcoming' || $count == 0) {
		if (!empty($variables['field_promo_videos'])) {
			$variables['show_promo'] = TRUE;
		}
	}


	// variables for node--manga.tpl.php
	$variables['chapters'] = '';
	$variables['link_to_anime'] = 'false';

	/*
	 * Volumes and Chapters display setup
	 */
	if (!empty($variables['field_number_of_volumes'])) {
		if ($variables['field_number_of_volumes'][0]['value'] == 0) {
			// if volume number is set to 0, the display is unknown
			$variables['volumes'] = '<div class="field-label">Volume(s): </div><div class="field-items">unknown</div>';
		} else {
			$variables['volumes'] = '<div class="field-label">Volume(s): </div><div class="field-items">' . $variables['field_number_of_volumes'][0]['value'] . '</div>';
		}
	}
	if (!empty($variables['field_number_of_chapters'])) {
		$variables['chapters'] = '<div class="field-label">Chapter(s): </div><div class="field-items">' . $variables['field_number_of_chapters'][0]['value'] . '</div>';
	}

	/*
	 * Create link to related anime if it exists
	 */
	if (!empty($variables['field_anime'])) {
		$nid = $variables['field_anime'][0]['entity']->nid;
		$path = drupal_get_path_alias('node/' . $nid);
		$variables['link_to_anime'] = base_path() . $path;
	}
}

/**
 * Implements hook_preprocess_block()
 */
function ripple_theme_preprocess_block(&$variables) {
	// get featured artwork from theme setting
	$variables['featured_piece'] = '';
	$variables['featured_image_path'] = '';
	if (!empty(theme_get_setting('featured_artwork'))) {
		$id = theme_get_setting('featured_artwork');
		$variables['featured_piece'] = theme('image', array(
			'path'  => file_create_url(file_load($id)->uri),
			'alt'   => t('Featured Artwork'),
			'title' => t('Featured Artwork'),
			'type'
		));
		$variables['featured_image_path'] = file_create_url(file_load($id)->uri);
	}
}

/**
 * Implements hook_block_view_alter()
 */
function ripple_theme_block_view_alter(&$data, $block) {
	if ($block->module == 'user' && $block->delta == 'login') {
		$sitename = variable_get('site_name', "Default site name");
		$data['subject'] = t('Sign in to @sitename', array('@sitename' => $sitename));
	}
}

/**
 * Implements hook_menu_local_tasks_alter()
 */
function ripple_theme_menu_local_tasks_alter(&$data, $router_item, $root_path) {
	if (!empty($data['tabs'])) {
		$tabs = $data['tabs'][0]['output'];
    // 'user/%/edit'
		$paths = array('user/%/shortcuts','user/%/track/navigation','user/%/devel');
		if (!empty($tabs)) {
			foreach ($tabs as $key => $tab) {
				if (in_array($tab['#link']['path'],$paths)) {
					unset($data['tabs'][0]['output'][$key]);
				}
				if ($tab['#link']['path'] == 'user/%/view') {
					$data['tabs'][0]['output'][$key]['#link']['title'] = t('profile');
				}
			}
		}
	}
}

/**
 * Returns HTML for a breadcrumb trail.
 *
 * @param $variables
 *   An associative array containing:
 *   - breadcrumb: An array containing the breadcrumb links.
 */
function ripple_theme_breadcrumb($variables) {
  $breadcrumb = $variables['breadcrumb'];
  // dpm($breadcrumb);

  if (!empty($breadcrumb)) {
  	$breadcrumb[] = drupal_get_title();
    // Provide a navigational heading to give context for breadcrumb links to
    // screen-reader users. Make the heading invisible with .element-invisible.
    $output = '<h2 class="element-invisible">' . t('You are here') . '</h2>';

    $output .= '<div class="breadcrumb">' . implode(' ++ ', $breadcrumb) . '</div>';
    
    return $output;
  }
}

