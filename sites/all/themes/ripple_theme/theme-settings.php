<?php
/**
 * Implements hook_form_FORM_ID_alter().
 *
 * @param $form
 *   The form.
 * @param $form_state
 *   The form state.
 */
function ripple_theme_form_system_theme_settings_alter(&$form, &$form_state) {

  /*
   * Featured Artwork.
   */

    $form['ripple_theme']['artwork'] = array(
      '#type' => 'fieldset',
      '#title' => t('Featured Artwork'),
      '#description' => t('The Featured Artwork that is to be used throughout the site as a background image (landing screen, discover top bar, etc.).'),
    );

    $form['ripple_theme']['artwork']['featured_artwork'] = array(
        '#type'     => 'managed_file',
        '#title'    => t('Featured Artwork'),
        '#description' => t('Upload the fan artwork that will be featured on Ripple.'),
        '#progress_indicator' => 'bar',
        '#required' => FALSE,
        '#upload_location' => file_default_scheme() . '://featured/',
        '#default_value' => theme_get_setting('featured_artwork'),
        '#upload_validators' => array(
          'file_validate_extensions' => array('gif png jpg jpeg'),
        ),
    );

    $form['ripple_theme']['artwork']['artist_username'] = array(
        '#type'     => 'textfield',
        '#title'    => t('Artist\'s DeviantArt Userame'),
        '#description' => t('Enter the artist\'s DeviantArt username for the featured work'),
        '#required' => FALSE,
        '#default_value' => theme_get_setting('artist_username'),
    );

    $form['#submit'][] = 'ripple_theme_settings_form_submit';

    /*
     * The below lines are for the submit handler and are required to permanently save managed file
     */
    // Get all themes.
    $themes = list_themes();
    // Get the current theme
    $active_theme = $GLOBALS['theme_key'];
    $form_state['build_info']['files'][] = str_replace("/$active_theme.info", '', $themes[$active_theme]->filename) . '/theme-settings.php';
}

function ripple_theme_settings_form_submit(&$form, $form_state) {
  // save the managed file status to permanent
  $image_fid = $form_state['values']['featured_artwork'];
  $image = file_load($image_fid);
  if (is_object($image)) {
    // Check to make sure that the file is set to be permanent.
    if ($image->status == 0) {
      // Update the status.
      $image->status = FILE_STATUS_PERMANENT;
      // Save the update.
      file_save($image);
      // Add a reference to prevent warnings.
      file_usage_add($image, 'ripple_theme', 'theme', 1);
    }
  }

  variable_set('featured_artist_name', $form_state['values']['artist_username']);
}

