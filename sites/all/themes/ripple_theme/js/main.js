(function ($) {
	Drupal.behaviors.ripple = {
		attach: function (context, settings) {
			$( '.left-sidebar, .right-sidebar' ).matchHeight();
			$( '.view-id-anime .item-list li.views-row' ).matchHeight();
			$( '.view-id-manga .item-list li.views-row' ).matchHeight();
			$( '.view-users .views-row' ).matchHeight();
			$( '.privatemsg-message .privatemsg-author-avatar, .privatemsg-message .privatemsg-message-column' ).matchHeight();
			$( '.privatemsg-message .message-body, .privatemsg-message .privatemsg-message-datetime-holder' ).matchHeight();
			$( '.ui-accordion-content .views-row' ).matchHeight();

			$( '#sign-up' ).click(function() {
			    $( 'html, body' ).animate({
			        scrollTop: $( '#block-formblock-user-register' ).offset().top
			    }, 1000);
			});

			// position footer at bottom of the page if the window height is bigger than the body height
			$(window).bind("load", function() { 
				var footerHeight = 0,
				footerTop = 0,
				$footer = $( 'footer' );

				positionFooter();
				function positionFooter() {
					footerHeight = $footer.height();
					footerTop = ($(window).scrollTop()+$(window).height()-footerHeight)+"px";
					if ( $(document.body).height() < $(window).height()) {
						$footer.css({
							position: "absolute",
							left: 0,
							right: 0,
						}).animate({
							top: footerTop,
						})
					} else {
						$footer.css({
							position: "static"
						})
					}
				}
				$(window).scroll(positionFooter).resize(positionFooter);
			});

			$( '#block-formblock-user-register #edit-picture' ).matchHeight();
			$( '#block-formblock-user-register #edit-picture legend' ).remove();
			// $( '#block-formblock-user-register #edit-picture legend .file-icon' ).remove();
			$( '#block-formblock-user-register #edit-picture span.file' ).remove();

			// $( '.view-display-id-newest_anime_episodes .views-row .views-field-title' ).matchHeight();
			$( '.view-id-discover_anime .views-row .views-field-title' ).matchHeight();
			$( '.view-id-manga_blocks .views-row .views-field-title' ).matchHeight();

			$( '.view-display-id-watch .views-field.views-field-view-node, .view-display-id-watch .views-field.views-field-view-node-1, .view-display-id-watch .views-field.views-field-field-episode' ).matchHeight();

			$( '.featured-artwork .user-info' ).flexVerticalCenter();
			$( '.front.not-logged-in .featured-artwork .featuredart-title' ).flexVerticalCenter();
			$( '.privatemsg-message .privatemsg-message-datetime-holder .center-datetime' ).flexVerticalCenter();
			$( '.user-links > div' ).flexVerticalCenter();
			$( '.rate-create' ).flexVerticalCenter();
			$( '.featured-DA-artist p' ).flexVerticalCenter();

			// $( '.view-display-id-newest_anime_episodes .views-row .views-field-title .field-content' ).flexVerticalCenter();
			$( '.view-id-discover_anime .views-row .field-content' ).flexVerticalCenter();
			$( '.view-id-manga_blocks .views-row .field-content' ).flexVerticalCenter();

			$( '.view-display-id-watch .views-field.views-field-view-node .field-content, .view-display-id-watch .views-field.views-field-view-node-1 .field-content' ).flexVerticalCenter();

			$( '.view-users .views-field-ops .flag-friend a' ).html('');
			$( '.view-users .views-field-friend-link .flag-friend a' ).html('');
			$( '.view-users .views-field-view-user-1 a' ).html('');

			$( '.view-id-recommendations .views-field-view-node a, .view-id-review .views-field-view-node a' ).html('');
			$( '.view-id-recommendations .views-field-edit-node a, .view-id-review .views-field-edit-node a' ).html('');
			$( '.view-id-recommendations .views-field-delete-node a, .view-id-review .views-field-delete-node a' ).html('');

			$( '#main-menu .menu > li' ).each(function( index ) {
				if ($( this ).find( 'ul' ).width() < $( this ).width()) {
					// subtract px width to counter the border width
					$( this ).find( 'ul' ).width( $( this ).width() - 2 );
				}

				if ($( this ).hasClass( 'menu-user-profile-link' )) {
					if ($( this ).find( 'ul' ).width() > $( this ).width()) {
						$( this ).width( $( this ).find( 'ul' ).width() + 2 );
					}
				}
			});

		    $( '#sign-in' ).click(function() {
				$( '#sign-in').toggleClass('active');
				$( '#block-user-login--2').toggleClass('visible');
				$( '#login-form').toggleClass('visible');
			});
		}
	}
	Drupal.behaviors.dropkick = {
		attach: function (context, settings) {
			// all selects not fivestar selects
			$( 'form:not(.fivestar-widget) select' ).dropkick({
				mobile: true
			});
		}
	}
}(jQuery));