<?php
/**
 * @file
 * Code for the ripple_manga_read module.
 */

/*
 * Helper function to get volumes and chapters for the image navigation
 *
 * @params $nid
 *         the current manga id
 * @params $vol_num
 *         the current volume number
 * @params $ch_num
 *         the current chapter number
 * @params $vol_op
 *         the operator to select volumes from the database
 *         defaults to '='
 * @params $ch_op
 *         the operator to select chapters from the database
 *         defaults to '>'
 * @params $order
 *         the operator to select which order the volumes should be sorted by
 *         defaults to ascending order
 */
function _get_chapters_for_nav($nid,$vol_num,$ch_num,$vol_op = '=',$ch_op = '>',$order = 'ASC') {
  $query = db_select('field_data_field_manga','m');
  $query->join('field_data_field_chapter_number', 'ch_num', 'm.entity_id = ch_num.entity_id');
  $query->join('field_data_field_select_volume', 'vol', 'm.entity_id = vol.entity_id');
  $query->join('field_data_field_volume_number', 'vol_num', 'vol.field_select_volume_target_id = vol_num.entity_id');
  $query->condition('m.field_manga_target_id',$nid)
    ->condition('vol_num.field_volume_number_value',$vol_num,$vol_op)
    ->condition('ch_num.field_chapter_number_value',$ch_num,$ch_op)
    ->fields('m', array('bundle','entity_id'))
    ->fields('ch_num', array('field_chapter_number_value'))
    ->fields('vol', array('field_select_volume_target_id'))
    ->fields('vol_num', array('field_volume_number_value'))
    ->orderBy('vol_num.field_volume_number_value', $order)
    ->orderBy('ch_num.field_chapter_number_value', $order)
    ->range(0,1);
  $result = $query->execute();
  return $result;
}

/*
 * Helper function to count the number of pages in the current chapter
 *
 * @params $nid
 *         the chapter id
 */
function _count_chapter_pages($nid) {
  $query = db_select('field_data_field_collection_manga_pages','m');
  $query->condition('m.entity_id',$nid)
    ->fields('m', array('entity_id'));
  $result = $query->execute();
  return $result;
}

/*
 * Helper function to find the link for the last page
 */
function _last_link($page_num, $total_rows, $args) {
  if ($page_num == $total_rows) {
    $next_args = NULL;
    // $args[0] = manga id, $args[1] = volume number, $args[2] = chapter number
    $chapters = _get_chapters_for_nav($args[0],$args[1],$args[2]);
    // if there are no more chapters after the current chapter in the current volume
    if ($chapters->rowCount() == 0) {
      // check for chapters in volumes that are greater than the current volume
      $chapters = _get_chapters_for_nav($args[0],$args[1],0,'>');
    }
    // only one result but loop through the results to get the values
    foreach ($chapters as $chapter) {
      // next arguments to be used in the next url : /{vol_num}/{ch_num} (ex. /12/4)
      $next_args = '/' . $chapter->field_volume_number_value . '/' . $chapter->field_chapter_number_value; 
    }
    return $next_args;
  } else {
    return '';
  }
}

/*
 * Helper function to find the link for the first page
 */
function _first_link($page_num, $args) {
  if ($page_num == 1) {
    $prev_args = NULL;
    // $args[0] = manga id, $args[1] = volume number, $args[2] = chapter number
    // equal to the current volume, less than current chapter, in descending order
    $chapters = _get_chapters_for_nav($args[0],$args[1],$args[2],'=','<','DESC');
    // if there are no more chapters before the current chapter in the current volume
    if ($chapters->rowCount() == 0) {
      // check for chapters in volumes that are less than the current volume in descending order
      $chapters = _get_chapters_for_nav($args[0],$args[1],0,'<','>','DESC');
    }
    // only one result but loop through the results to get the values
    foreach ($chapters as $chapter) {
      // count the pages in the chapter
      $pages = _count_chapter_pages($chapter->entity_id);
      // if there is more than one page
      if ($pages->rowCount() > 1) {
        // add the page nav to the url (row count - 1 to get the delta for the page)
        $page = '?page=' . ($pages->rowCount()-1);
      } else {
        // if row count is 0 or 1 don't use page variable
        $page = '';
      }
      // prev arguments to be used in the prev url : /{vol_num}/{ch_num} (ex. /12/4) or /{vol_num}/{ch_num}?page={delta} (ex. /12/4?page=1)
      $prev_args = '/' . $chapter->field_volume_number_value . '/' . $chapter->field_chapter_number_value . $page;
    }
    return $prev_args;
  } else {
    return '';
  }
}

/**
 * Implementation of hook_views_pre_render().
 */
function ripple_manga_read_views_pre_render(&$view) {
  GLOBAL $base_url;
  if ($view->name == 'chapters' && $view->current_display == 'read') {
    // get the view contextual arguments
    $args = $view->args;
    // get the number of results (total number of pages)
    $total_rows = $view->total_rows;
    // set url parameter var to navigate pages
    $page = '?page=';
    // default pages to empty
    $prev_page = '';
    $next_page = '';

    // if the page url parameter exists
    if (isset($_GET['page'])) {
      // set the page as the delta from the url
      $this_page = $_GET['page'];
      // nav pages is one less and one more than the current
      $prev_page = $page . ($this_page-1);
      $next_page = $page . ($this_page+1);

      // if this is the first page remove the page url parameter
      if ($this_page == 1) {
        $prev_page = '';
      }

      // set the nav links to use the base arguments and the url parameter
      $prev_link = 'href="' . $base_url . '/manga/read/' . implode('/',$args) . $prev_page . '"';
      $next_link = 'href="' . $base_url . '/manga/read/' . implode('/',$args) . $next_page . '"';
    } else {
      // if the url parameter doesn't exist set the delta to 0
      $this_page = 0;
      // prev link is empty by default
      $prev_link = '';
      // set the next page url parameter to be one more than the current
      $next_page = $page . ($this_page+1);
      // set the next link to use the base arguments and the url parameter
      $next_link = 'href="' . $base_url . '/manga/read/' . implode('/',$args) . $next_page . '"';
    }

    // if first page of the chapter
    if (($this_page+1) == 1) {
      // check for more chapters before the current
      $first = _first_link(1,$args);
      // if the result isn't empty
      if (!empty($first)) {
        // create link to previous chapter and use the returned arguments from _first_link()
        $prev_link = 'href="' . $base_url . '/manga/read/' . $args[0] . $first . '"';
      } else {
        // no link if no results
        $prev_link = '';
      }
    }

    // if page number equals the total number of results
    if (($this_page+1) == $total_rows) {
      // check for more chapters after the current
      $last = _last_link(($this_page+1), $total_rows, $args);
      // if the result isn't empty
      if (!empty($last)) {
        // create link to next chapter and use the returned arguments from _last_link()
        $next_link = 'href="' . $base_url . '/manga/read/' . $args[0] . $last . '"';
      } else {
        // no link if no results
        $next_link = '';
      }
    }
    
    // set the image pager to use the navigation set from above
    $view->query->pager->display->handler->handlers['field']['nothing']->options['alter']['text'] = '<a class="previous-page" ' .$prev_link . '></a><a class="next-page" ' . $next_link . '></a>';

    // if there are no pages unset the image navigation
    if (empty($view->result[0]->field_field_page_number)) {
      unset($view->query->pager->display->handler->handlers['field']['nothing']);
    }

    // get no results behaviour for the manga page field
    $no_results = $view->query->pager->display->handler->handlers['field']['field_manga_page']->options['empty']; 
    // create previous chapter link with the existing prev link for the image navigation
    $previous = '<a class="previous-page" ' .$prev_link . '>Previous Chapter</a>';
    // create next chapter link with the existing next link for the image navigation
    $next = '<a class="next-page" ' . $next_link . '>Next Chapter</a>';
    // if first is empty remove the previous chapter link
    if (empty($first)) {
      $previous = '';
    }
    // if last is empty remove the next chapter link
    if (empty($last)) {
      $next = '';
    }
    // create the chapter nav div to add to the no results behaviour
    $chapter_nav = '<div class="chapter-nav">' . $previous . $next . '</div>';
    // add the chapter nav to the current no result text and wrap in a div
    $view->query->pager->display->handler->handlers['field']['field_manga_page']->options['empty'] = '<div class="no-pages">' . $no_results . $chapter_nav . '</div>';
  }
}

function ripple_manga_read_preprocess_views_view_fields(&$vars) {
  if ($vars['view']->name == 'chapters' && $vars['view']->current_display == 'read') {    
    /*
     * Container for the chapter pages and navigation
     */
    if (isset($vars['fields']['nothing'])) {
      $field = 'nothing';
    } else {
      $field = 'field_manga_page';
    }

    $wrapper_prefix = $vars['fields']['field_manga_page']->wrapper_prefix;
    $wrapper_suffix = $vars['fields'][$field]->wrapper_suffix;

    $vars['fields']['field_manga_page']->wrapper_prefix = '<div class="read">' . $wrapper_prefix;
    $vars['fields'][$field]->wrapper_suffix = '</div>' . $wrapper_suffix;

    $wrapper_prefix = $vars['fields']['field_rate']->wrapper_prefix;
    $wrapper_suffix = $vars['fields']['created']->wrapper_suffix;
    $vars['fields']['field_rate']->wrapper_prefix = '<div class="rate-create">' . $wrapper_prefix;
    $vars['fields']['created']->wrapper_suffix = '</div>' . $wrapper_suffix;

    /*
     * Container for the
     */
    if (isset($vars['fields']['field_page_number'])) {
      $field = 'field_page_number';
    } else {
      $field = 'field_chapter_number';
    }

    $holder_prefix = $vars['fields']['title']->wrapper_prefix;
    $holder_suffix = $vars['fields'][$field]->wrapper_suffix;
    $vars['fields']['title']->wrapper_prefix = '<div class="chapter_info">' . $holder_prefix;
    $vars['fields'][$field]->wrapper_suffix = '</div>' . $holder_suffix;

    $wrapper_prefix = $vars['fields']['field_volume_number']->wrapper_prefix;
    $wrapper_suffix = $vars['fields'][$field]->wrapper_suffix;
    $vars['fields']['field_volume_number']->wrapper_prefix = '<div class="chapter-specs">' . $wrapper_prefix;
    $vars['fields'][$field]->wrapper_suffix = $wrapper_suffix . '</div>';
  }
}
