<?php

function kint($vars) {
  dpm($vars);
}

/**
 * Get all episodes between two numbers
 * 
 * @param  integer $nid        the ID of the referenced Anime
 * @param  integer $s          the season number
 * @param  integer $first_ep   min episode number
 * @param  integer $last_ep    max episode number
 * 
 * @return array
 */
function _getAllEpisodesInRange($nid,$s = 1,$first_ep = 1,$last_ep = 5) {
  $query = db_select('field_data_field_anime','anime');
  $query->join('field_data_field_episode_number', 'ep', 'anime.entity_id = ep.entity_id');
  $query->join('field_data_field_season_number', 's', 'anime.entity_id = s.entity_id');
  $query->condition('anime.field_anime_target_id',$nid)
    ->condition('s.field_season_number_value',$s)
    ->condition('ep.field_episode_number_value',array($first_ep,$last_ep),'BETWEEN')
    ->fields('s')
    ->fields('ep')
    ->fields('anime', array('field_anime_target_id'));
  $result = $query->execute();
  return $result->fetchAll();
}

/**
 * Get an Anime's season's episodes
 * 
 * @param  integer $nid    the ID of the referenced Anime
 * @param  integer $s      the season number
 * @param  string  $op     the operator ('>', '<', or '=')
 * @param  string  $order  the order of the results ('ASC' or 'DESC')
 * 
 * @return array
 */
function _getSeason($nid,$s,$op = '>',$order = 'ASC') {
  $query = db_select('field_data_field_anime','anime');
  $query->join('field_data_field_season_number', 's', 'anime.entity_id = s.entity_id');
  $query->condition('anime.field_anime_target_id',$nid)
    ->condition('s.field_season_number_value',$s,$op)
    ->fields('s')
    ->fields('anime', array('field_anime_target_id'))
    ->range(0,1)
    ->orderBy('s.field_season_number_value',$order);

  $result = $query->execute();
  return $result->fetchAll();
}

/**
 * Get an episode from a specific anime and season
 * 
 * @param  integer $nid    the ID of the referenced Anime
 * @param  integer $s      the season number
 * @param  integer $ep     the episode number
 * @param  string  $op     the operator ('>', '<', or '=')
 * @param  string  $order  the order of the results ('ASC' or 'DESC')
 * 
 * @return array
 */
function _getEpisode($nid,$s,$ep,$op = '=',$order = 'ASC') {
  $query = db_select('field_data_field_anime','anime');
  $query->join('field_data_field_season_number', 's', 'anime.entity_id = s.entity_id');
  $query->join('field_data_field_episode_number', 'ep', 'anime.entity_id = ep.entity_id');
  $query->condition('anime.field_anime_target_id',$nid)
    ->condition('s.field_season_number_value',$s)
    ->condition('ep.field_episode_number_value',$ep,$op)
    ->fields('ep')
    ->fields('s')
    ->fields('anime', array('field_anime_target_id'))
    ->orderBy('ep.field_episode_number_value', $order)
    ->range(0,1);

  $result = $query->execute();
  return $result->fetchAll();
}

/**
 * Get the episodes in a certain season of an Anime
 * 
 * @param  integer      $nid     the ID of the referenced Anime
 * @param  integer      $s       the season number
 * @param  string       $order   the order of the results ('ASC' or 'DESC')
 * @param  integer|NULL $limit   limit results returned
 * 
 * @return array
 */
function _getEpisodesInSeason($nid,$s,$order = 'ASC',$limit = NULL) {
  $query = db_select('field_data_field_anime','anime');
  $query->join('field_data_field_season_number', 's', 'anime.entity_id = s.entity_id');
  $query->join('field_data_field_episode_number', 'ep', 'anime.entity_id = ep.entity_id');
  $query->condition('anime.field_anime_target_id',$nid)
    ->condition('s.field_season_number_value',$s)
    ->fields('ep')
    ->fields('s')
    ->fields('anime', array('field_anime_target_id'))
    ->orderBy('ep.field_episode_number_value', $order);
  if (isset($limit)) {
    $query->range(0,$limit);
  }

  $result = $query->execute();
  return $result->fetchAll();
}

/**
 * Get an episodes thumbnail
 * 
 * @param  integer $nid  the nid of the episode 
 * 
 * @return array
 */
function _getThumbnail($nid) {
  $query = db_select('field_data_field_episode','ep');
  $query->condition('ep.entity_id',$nid)
    ->fields('ep');

  $result = $query->execute();
  return $result->fetchAll();
}

/**
 * Get start and end values for view filters
 * 
 * @param  array    $this_s_eps   episodes of the current season
 * @param  integer  $nid          the nid of the Anime
 * @param  integer  $s            the season number
 * @param  integer  $this_ep      the episode number
 * @param  integer  $start        current start value of filter
 * @param  integer  $last         current end value of filter
 * @param  string   $op           previous or next filter
 * 
 * @return array|NULL
 */
function _getFilterValues($this_s_eps,$nid,$s,$this_ep,$start = 1,$last = 5,$op = 'prev') {
  if (count($this_s_eps) < 5) {
    $diff = (5 - count($this_s_eps));
    if ($op == 'prev') {
      $prev_s_eps = _getEpisodesInSeason($nid,$s,'DESC',1);
      $diff = (5 - count($this_s_eps) - 1);
      $start = ($prev_s_eps[0]->field_episode_number_value - $diff);
      return array('start' => $start,'last' => $prev_s_eps[0]->field_episode_number_value);
    } else {
      $next_s_eps = _getAllEpisodesInRange($nid,$s,1,$diff);
      if ((count($next_s_eps) + count($this_s_eps)) < 5) {
        $diff = (count($next_s_eps) + count($this_s_eps));
        $start = $start - (5 - $diff);
        return $start;
      }
    }
  }
  
  return;
}


